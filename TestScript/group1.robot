*** Settings ***
Library           Selenium2Library
Library           Process

*** Test Cases ***
TC01
    Run Process    taskkill -f -im chromedriver.exe    shell=True
    Open Browser    http://uat-stardust.officemate.co.th/    chrome
    Input text    username    surachart
    Input text    password    officemate
    Click Button    //button[@type="submit"]
    Wait Until Element Is Visible    id=44_active
    Click Element    id=44_active
    Click Element    //a[@href="/SO/Normal"]
    Wait Until Element Is Visible    //button[@class="btn btn-create-doc btn-size-default"]
    Click Element    //button[@class="btn btn-create-doc btn-size-default"]
    Wait Until Element Is Visible    //input[@placeholder="ชื่อผู้ติดต่อ/ชื่อลูกค้า/รหัสลูกค้า/ที่อยู่/เบอร์โทร/อีเมล"]    2s
    Input text    //input[@placeholder="ชื่อผู้ติดต่อ/ชื่อลูกค้า/รหัสลูกค้า/ที่อยู่/เบอร์โทร/อีเมล"]    11111173
    Click Element    //div[@class="input-group-addon addon-default pointer"]
    Wait Until Element Is Visible    //div[@class="select-customer-box"]
    Click Element    //div[@class="select-customer-box"]
    Wait Until Element Is Visible    //button[@class="btn-next btn btn-block btn-delete btn-m"]
    Click Element    //button[@class="btn-next btn btn-block btn-delete btn-m"]
    sleep    2s
    Input text    //input[@placeholder="รหัสสินค้า*จำนวน"]    5010111*5
    sleep    2s
    Press Key    //input[@placeholder="รหัสสินค้า*จำนวน"]    \\13
    Wait Until Element Is Visible    //td[@class="text-center"]
    Wait Until Element Is Visible    //button[@class="btn-next btn btn-block btn-delete btn-m"]
    Click Element    //button[@class="btn-next btn btn-block btn-delete btn-m"]
    Wait Until Page Contains    เลือกของแถม,ใช้คูปองส่วนลด
    Wait Until Element Is Visible    //button[@class="btn-next btn btn-block btn-delete btn-m"]
    Click Element    //button[@class="btn-next btn btn-block btn-delete btn-m"]
    sleep    2s
    Select From List By Value    //select[@name="ประเภทการสั่งซื้อ"]    1
    Click Element    //input[@name="PO"]
    Wait Until Element Is Visible    //button[@class="btn-next btn btn-block btn-delete btn-m"]
    Click Element    //button[@class="btn-next btn btn-block btn-delete btn-m"]
    sleep    2s
    Wait Until Element Is Visible    //button[@class="btn-next btn btn-block btn-delete btn-m"]
    Click Element    //button[@class="btn-next btn btn-block btn-delete btn-m"]
    sleep    2s
    Click Element    xpath=(//button[@class="btn btn-success"])[2]
