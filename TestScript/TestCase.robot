*** Settings ***
Library           Selenium2Library
Library           Process
Library           Database Library
Library           Suds Library

*** Test Cases ***
Create Order Customer Personal status "Active"
    Open Browser And Log In
    Login Username & Password    surachart    officemate
    Go To 'เอกสาร' Menu
    Go To 'ใบสั่งจัด SO ปกติ' Screen
    Create 'เอกสาร'
    Verify Page 'ลูกค้า'
    Search Customer Data By Customer ID    11111173
    Select Customer By Customer ID And Expected Statu    11111173    Active
    Click Next
    Verify Page 'รายการสินค้า'
    Search And Select Product By Product Id    5010111    5
    Click Next
    Verify Page 'ของแถม'
    Click Next
    Verify Page 'ข้อมูลอื่นๆ'
    Input Datat Page 'ข้อมูลอื่นๆ'    1    Complete
    Click Next
    Verify Summary Page    11111173    5
    Click 'สร้างเอกสาร'
    Click 'ยืนยันสร้างเอกสาร'
    Verify After Confirm     Open

Test If
    ${True}    Set Variable    True11235
    Run Keyword If    '${True}' == 'True'    Log    PASS
    ...    ELSE    Log     Test FAIL

Test FOR
    :FOR    ${index}    IN    RANGE    10
    \    Log    ${index}

Test DB
    Connect To Database
    @{result}    Query    SELECT * FROM person
    Log Many    @{result}
    ${queryResults}    Query    SELECT first_name, last_name FROM person
    Log    ${queryResults[0][1]}, ${queryResults[0][0]}
    Disconnect From Database

Test Request
    Create Soap Client
    Set Http Authentication

*** Keywords ***
Open Browser And Log In
    Run Process    taskkill -f -im chromedriver.exe    shell=True
    # Test Step
    Open Browser    http://uat-stardust.officemate.co.th/Login    chrome
    Maximize Browser Window
    # Verify
    Title Should Be    OASYS
    Element Should Be Visible    name=username
    Element Should Be Visible    name=password
    Page Should Contain Button    //button[text()='เข้าสู่ระบบ']
    Element Should Be Visible    //span[text()='ลืมรหัสผ่าน?']    # Page Should Contain Link ,Not Support
    Page Should Contain Checkbox    //span[text()='จำการเข้าสู่ระบบ']/preceding::input[@type='checkbox']

Login Username & Password
    [Arguments]    ${user_name}    ${password}
    Input Text    name=username    ${user_name}
    Input Text    name=password    ${password}
    Click Element    //button[text()='เข้าสู่ระบบ']
    Wait Until Element Is Visible    //div[@class='navbar']//*[normalize-space(text())='เอกสาร']    # ตรวจสอบเมนู "เอกสาร"

Go To 'เอกสาร' Menu
    Mouse Over    //div[@class='navbar']//*[normalize-space(text())='เอกสาร']
    # Header Title Should Be Visible
    Wait Until Element Is Visible    //div[@class="title-header"][text()='ใบสั่งจัด']
    Wait Until Element Is Visible    //div[@class="title-header"][text()='ใบเสนอราคา']
    Wait Until Element Is Visible    //div[@class="title-header"][text()='ใบค่าบริการ (EO)']

Go To 'ใบสั่งจัด SO ปกติ' Screen
    Wait Until Element Is Visible    //a[normalize-space(text())='ใบสั่งจัด(SO) แบบปกติ']
    Click Element    //a[normalize-space(text())='ใบสั่งจัด(SO) แบบปกติ']
    # Verify
    Wait Until Page Contains    รายการใบสั่งจัด (SO)
    Element Should Be Visible    //ul[@role="tablist"]//a[text()='ค้นหา']
    Element Should Be Visible    //ul[@role="tablist"]//a[text()='ค้นหาเพิ่มเติม']
    Page Should Contain Button    //button[text()='เริ่มสร้างเอกสาร']

Create 'เอกสาร'
    Click Element    //button[text()='เริ่มสร้างเอกสาร']
    Wait Until Element Is Visible    //li//*[text()='ลูกค้า']

Search Customer Data By Customer ID
    [Arguments]    ${customer_id}
    Input Text    //input[@placeholder='ชื่อผู้ติดต่อ/ชื่อลูกค้า/รหัสลูกค้า/ที่อยู่/เบอร์โทร/อีเมล']    ${customer_id}
    Click Element    //input[@placeholder='ชื่อผู้ติดต่อ/ชื่อลูกค้า/รหัสลูกค้า/ที่อยู่/เบอร์โทร/อีเมล']/parent::*//i[@class='fa fa-search']
    Wait Until Element Is Visible    //div[@class='select-customer-box']

Select Customer By Customer ID And Expected Statu
    [Arguments]    ${customer_id}    ${expected_status}
    Wait Until Element Is Visible    //div[@class='select-customer-id'][text()='${customer_id}']
    Click Element    //div[@class='select-customer-id'][text()='${customer_id}']
    # Verify
    Wait Until Element Is Visible    //li[@class='timeline-item active']//*[text()='ลูกค้า']
    Wait Until Element Is Visible    //li[@class='timeline-item active']//*[text()='ที่อยู่จัดส่ง,ผู้ติดต่อ']
    Wait Until Element Is Visible    //li[@class='timeline-item']//*[text()='รายการสินค้า']
    Wait Until Element Is Visible    //li[@class='timeline-item']//*[text()='ของแถม']
    Wait Until Element Is Visible    //li[@class='timeline-item']//*[text()='ข้อมูลอื่นๆ']
    # Verify Customer Data
    Element Should Be Visible    //div[@class='select-customer-id'][text()='${customer_id}']
    Element Should Be Visible    //span[@class='label-status label-active'][normalize-space(text())='${expected_status}']

Click Next
    Mouse Over    //button//i[@class='fa fa-chevron-right']
    Wait Until Element Is Visible    //button//span[text()='ขั้นตอนถัดไป']
    Click Element    //button//span[text()='ขั้นตอนถัดไป']

Search And Select Product By Product Id
    [Arguments]    ${product_id}    ${product_quantity}
    Sleep    2s
    Input Text    //input[@placeholder='รหัสสินค้า*จำนวน']    ${product_id}*${product_quantity}
    Wait Until Element Is Visible    //div[@class='item']//*[contains(.,'${product_id}')]
    Press Key    //input[@placeholder='รหัสสินค้า*จำนวน']    \\13
    # Verify After Input Product
    Wait Until Element Is Visible    //table[@class='table table-report td-middle']
    Table Cell Should Contain    //table[@class='table table-report td-middle']    2    2    ${product_id}
    Table Cell Should Contain    //table[@class='table table-report td-middle']    2    3    ${product_quantity}
    Wait Until Element Is Visible    //table[@class='table table-report td-middle']/tbody//tr[1]//td[12]/button/i[@class='fa fa-times']    # Table Cell Should Contain, Not Support 'Icon'
    Wait Until Element Is Visible    //table[@class='table table-report td-middle'] /tbody//tr[1]//td[count(//th[text()='ค่าขนส่ง']/preceding-sibling::th)+1][text()='0.00']

Verify Page 'ลูกค้า'
    Wait Until Element Is Visible    //li[@class='timeline-item active']//*[text()='ลูกค้า']    # Element Should Be Enabled, Not Support
    Wait Until Element Is Visible    //li[@class='timeline-item']//*[text()='ที่อยู่จัดส่ง,ผู้ติดต่อ']    # Element Should Be Disabled, Not Support
    Wait Until Element Is Visible    //li[@class='timeline-item']//*[text()='รายการสินค้า']
    Wait Until Element Is Visible    //li[@class='timeline-item']//*[text()='ของแถม']
    Wait Until Element Is Visible    //li[@class='timeline-item']//*[text()='ข้อมูลอื่นๆ']
    Page Should Contain Textfield    //input[@placeholder='ชื่อผู้ติดต่อ/ชื่อลูกค้า/รหัสลูกค้า/ที่อยู่/เบอร์โทร/อีเมล']

Verify Page 'รายการสินค้า'
    Wait Until Page Contains    กรุณาเพิ่มรายการสินค้า
    Wait Until Element Is Visible    //input[@placeholder='รหัสสินค้า*จำนวน']

Verify Page 'ของแถม'
    Wait Until Page Contains    เลือกของแถม,ใช้คูปองส่วนลด

Verify Page 'ข้อมูลอื่นๆ'
    Wait Until Element Is Visible    name=ประเภทการสั่งซื้อ
    Page Should Contain Radio Button    //input[@value="InComplete"]
    Page Should Contain Radio Button    //input[@value="InComplete"]
    Page Should Contain Radio Button    //input[@value="None"]

Input Datat Page 'ข้อมูลอื่นๆ'
    [Arguments]    ${expected_type_order}    ${expected_PO}
    [Documentation]    - Complete : สินค้าครบ PO
    Select From List By Value    name=ประเภทการสั่งซื้อ    ${expected_type_order}
    Select Radio Button    PO    ${expected_PO}

Verify Summary Page
    [Arguments]    ${customer_id}    ${product_quantity}
    Wait Until Page Contains    ${customer_id}
    Wait Until Page Contains    ${product_quantity}

Click 'สร้างเอกสาร'
    Mouse Over    //button//i[@class='fa fa-chevron-right']
    Wait Until Element Is Visible    //button//span[text()='สร้างเอกสาร']
    Click Element    //button//span[text()='สร้างเอกสาร']

Click 'ยืนยันสร้างเอกสาร'
    Click Element    //div[@class='modal-content']//button[text()='ยืนยัน']

Verify After Confirm
    [Arguments]    ${expected_so_status}
    Wait Until Element Is Visible    //span[contains(@class,'label-status')][text()='${expected_so_status}']
