*** Settings ***
Library           Selenium2Library

*** Variables ***
${URL}            http://uat-stardust.officemate.co.th/
${BROWSER}        chrome

*** Test Cases ***
Create Order with Personal Cutomer
    [Tags]    PASS
    1st Open Login Page
    Maximize Browser
    Verify Valid Input Form
    2nd Input Form Data    surachart    officemate
    3rd Click Login Button
    4th Click Menu Create Order
    5th Beginning Create Order
    6th Search Customer from ID    11111173
    7th Verify Customer ID    11111173
    8th Verify Customer Status    Active
    9th Click Next Step
    10th Verify Product can be Input
    11th Input Product ID and Quantity    5010111    5
    12th Verify Product Added to Line
    13th Click Next Step
    14th Verify Data and Next Step
    15th Select the Type of Order
    16th Select the Delivery Date
    17th Select the PO Completed option
    18th Click Next Step
    19th Verify Data and Click Create Order
    20th Click Confirm Create Order

*** Keywords ***
1st Open Login Page
    Open Browser    ${URL}    ${BROWSER}
Maximize Browser
    Maximize Browser Window
Verify Valid Input Form
    Wait Until Element Is Visible    //input[@name="username"]
    Wait Until Element Is Visible    //input[@name="password"]
    Wait Until Element Is Visible    //button[@type="submit"]
2nd Input Form Data
    [Arguments]    ${Username}    ${Password}
    Input Text    name=username    ${Username}
    Input Text    name=password    ${Password}
3rd Click Login Button
    Click Element    //button[@type='submit']
4th Click Menu Create Order
    Wait Until Element Is Visible    id=44_active    10s
    Click Element    id=44_active
    Click Element    //a[@href="/SO/Normal"]
5th Beginning Create Order
    Wait Until Element Is Visible    //button[@class="btn btn-create-doc btn-size-default"]    10s
    Click Element    //button[@class="btn btn-create-doc btn-size-default"]
6th Search Customer from ID
    [Arguments]    ${CustomerID}
    Wait Until Element Is Visible    //input[@placeholder="ชื่อผู้ติดต่อ/ชื่อลูกค้า/รหัสลูกค้า/ที่อยู่/เบอร์โทร/อีเมล"]    10s
    Wait Until Element Is Visible    //div[@class="input-group-addon addon-default pointer"]    10s
    Input Text    //input[@placeholder="ชื่อผู้ติดต่อ/ชื่อลูกค้า/รหัสลูกค้า/ที่อยู่/เบอร์โทร/อีเมล"]    ${CustomerID}
    Click Element    //div[@class="input-group-addon addon-default pointer"]
7th Verify Customer ID
    [Arguments]    ${CustomerID}
    Wait Until Element Contains    class=select-customer-id    ${CustomerID}
8th Verify Customer Status
    [Arguments]    ${Status}
    Wait Until Element Contains    //span[@class="label-status label-active"]    ${Status}
    Click Element    class=select-customer-box
9th Click Next Step
    Wait Until Element Is Visible    //button[@class="btn-next btn btn-block btn-delete btn-m"]
    Click Element    //button[@class="btn-next btn btn-block btn-delete btn-m"]
10th Verify Product can be Input
    Sleep    3
    Wait Until Element Is Visible    //*[@id="fixed-search-product"]/div/div[1]/div/div[1]/div/div/input
    Wait Until Element Is Visible    //*[@id="fixed-search-product"]/div/div[1]/div/div[1]/div/div/div
11th Input Product ID and Quantity
    [Arguments]    ${ProductID}    ${Quantity}
    Input Text    //input[@placeholder="รหัสสินค้า*จำนวน"]    ${ProductID}*${Quantity}
    Sleep    3
    Click Element    (//div[@class="input-group-addon pointer"])[2]
12th Verify Product Added to Line
    Wait Until Element Contains    //*[@id="app"]/div[2]/div[2]/div[2]/div[2]/div/div/table/tbody/tr/td[2]/div/div    5010111
13th Click Next Step
    Sleep    3
14th Verify Data and Next Step
    Click Element    //button[@class="btn-next btn btn-block btn-delete btn-m"]
    Click Element    //button[@class="btn-next btn btn-block btn-delete btn-m"]
15th Select the Type of Order
    Sleep    3
    Wait Until Element Is Visible    //select[@name="ประเภทการสั่งซื้อ"]
    Click Element    //select[@name="ประเภทการสั่งซื้อ"]
    Click Element    //option[@value="1"]
16th Select the Delivery Date
    Click Element    //input[@class="dropdown-toggle form-control form-default"]
    Click Element    //button[@class="btn btn-info btn-sm"]
17th Select the PO Completed option
    Click Element    name=PO
18th Click Next Step
    Click Element    //button[@class="btn-next btn btn-block btn-delete btn-m"]
    Click Element    //button[@class="btn-next btn btn-block btn-delete btn-m"]
19th Verify Data and Click Create Order
    Sleep    3
    Mouse Over    //button[@class="btn-next btn btn-block btn-delete btn-m"]
    Click Element    //button[@class="btn-next btn btn-block btn-delete btn-m"]
20th Click Confirm Create Order
    Sleep    3
    Click Element    //*[@id="modal-create"]/div/div/div[2]/div/button