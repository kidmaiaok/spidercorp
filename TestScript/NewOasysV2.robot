*** Settings ***
Library           Selenium2Library
Library           Process

*** Test Cases ***
Open Browser
    Run Process    taskkill -f -im chromedriver.exe    shell=True
    Open Browser    http://uat-stardust.officemate.co.th/Login    chrome
    Maximize Browser Window
    #Verify URL
    Wait Until Element Is Visible    xpath=//*[@id="Login"]/div[2]/div/form/div/div[1]/img
    Page Should Contain    ลืมรหัสผ่าน?    #remember me
    Page Should Contain Element    xpath=//*[@id="Login"]/div[2]/div/form/div/div[6]/span    #remember me
    Title Should Be    OASYS    #title
    Element Should Be Visible    xpath=//*[@id="Login"]/div[2]/div/form/div/div[5]/span    #checkbox
    Page Should Contain Checkbox    xpath=//*[@id="Login"]/div[2]/div/form/div/div[5]/input
    Page Should Contain    จำการเข้าสู่ระบบ
    Page Should Contain Textfield    username
    Page Should Contain Textfield    password
    Page Should Contain Button    xpath=//*[@id="Login"]/div[2]/div/form/div/div[7]/button    #เข้าสู่ระบบ
    Input Text    username    surachart
    Input Password    password    officemate
    Click Button    xpath=//*[@id="Login"]/div[2]/div/form/div/div[7]/button
    #Page1
    Wait Until Page Contains Element    xpath=//*[@id="44_active"]

MenuAndVerifyData
    Sleep    2s
    Click Element    xpath=//a[@id="44_active"]
    Click Element    xpath=//a[@href="/SO/Normal"]
    Sleep    2s
    Click Element    xpath=//button[@class="btn btn-create-doc btn-size-default"]
    Wait Until Element Is Visible    xpath=//*[@id="app"]/div[2]/div[2]/div/div[1]/div/div/div/div/input
    Click Element    xpath=//input[@class="form-control form-default"]
    Click Element    xpath=//input[@placeholder="ชื่อผู้ติดต่อ/ชื่อลูกค้า/รหัสลูกค้า/ที่อยู่/เบอร์โทร/อีเมล"]
    Input Text    xpath=//*[@id="app"]/div[2]/div[2]/div/div[1]/div/div/div/div/input    11111173
    Sleep    2s
    Click Element    xpath=//div[@class="input-group-addon addon-default pointer"]
    Wait Until Element Is Visible    xpath=//*[@id="app"]/div[2]/div[2]/div/div[3]/div/div/div[4]/div[2]
    Click Element    xpath=//*[@id="app"]/div[2]/div[2]/div/div[3]/div/div/div[4]/div[2]
    Wait Until Element Is Visible    xpath=//span[@class="label-status label-active"]
    Element Text Should Be    xpath=//span[@class="label-status label-active"]    Active
    Sleep    2s
    Mouse Over    xpath=//i[@class="fa fa-chevron-right"]
    Wait Until Element Contains    xpath=//button[@class="btn-next btn btn-block btn-delete btn-m"]    ขั้นตอนถัดไป
    Click Element    xpath=//button[@class="btn-next btn btn-block btn-delete btn-m"]

AddProduct
    Sleep    2s
    Input Text    xpath=//*[@id="fixed-search-product"]/div/div[1]/div/div[1]/div/div/input    5010111*5
    Click Element    xpath=//div[@class="input-group-addon pointer"]
    Press Key    xpath=//*[@id="fixed-search-product"]/div/div[1]/div/div[1]/div/div/input    \\13
    Sleep    2s
    Input Text    xpath=//*[@id="fixed-search-product"]/div/div[1]/div/div[1]/div/div/input    3001348*5
    Click Element    xpath=//div[@class="input-group-addon pointer"]
    Press Key    xpath=//*[@id="fixed-search-product"]/div/div[1]/div/div[1]/div/div/input    \\13
    Sleep    3s
    Mouse Over    xpath=//i[@class="fa fa-chevron-right"]
    Wait Until Element Contains    xpath=//button[@class="btn-next btn btn-block btn-delete btn-m"]    ขั้นตอนถัดไป
    Click Element    xpath=//button[@class="btn-next btn btn-block btn-delete btn-m"]
    Page Should Contain    เลือกของแถม,ใช้คูปองส่วนลด

CreateDocID
    Click Element    xpath=//button[@class="btn-next btn btn-block btn-delete btn-m"]
    Sleep    2s
    Wait Until Element Is Visible    xpath=//*[@id="app"]/div[2]/div[2]/div[1]/div/div[2]/div[1]/div/div[3]/select/option[2]
    Click Element    xpath=//*[@id="app"]/div[2]/div[2]/div[1]/div/div[2]/div[1]/div/div[3]/select/option[2]
    Input Text    xpath=//*[@id="app"]/div[2]/div[2]/div[1]/div/div[2]/div[1]/div/div[4]/div/input    22/09/2018
    Sleep    2s
    Click Button    xpath=//*[@id="app"]/div[2]/div[2]/div[1]/div/div[2]/div[2]/div/div[1]/div[1]/label/input
    Wait Until Element Is Visible    xpath=//*[@id="app"]/div[2]/div[1]/div/div[3]/button
    Sleep    2s
    Click Element    xpath=//*[@id="app"]/div[2]/div[1]/div/div[3]/button
    Click Element    xpath=//*[@id="app"]/div[2]/div[1]/div/div[3]/button/span
    Wait Until Element Is Visible    xpath=//*[@id="app"]/div[2]/div[1]/div/div[3]/button
    Sleep    2s
    Click Element    xpath=//*[@id="app"]/div[2]/div[1]/div/div[3]/button
    Click Element    xpath=//*[@id="modal-create"]/div/div/div[2]/div/button
    Wait Until Element Is Visible    xpath=//*[@id="app"]/div[2]/div[1]/div/div/div[2]/input
    Capture Page Screenshot
